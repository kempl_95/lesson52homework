package kz.attractorschool.moviereviewrr.model;

import kz.attractorschool.moviereviewrr.util.Generator;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.util.UUID;

@Data
@Document(collection="reviews")
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor
public class Review {

    private static final Random r = new Random();

    @Id
    @Builder.Default
    @Getter
    @Setter
    private String id = UUID.randomUUID().toString();
    @Indexed
    @Getter
    @Setter
    private int stars;
    @Getter
    @Setter
    private String review;

    @DBRef
    @Indexed
    @Getter
    @Setter
    private User reviewer;

    @DBRef
    @Indexed
    @Getter
    @Setter
    private Movie movie;

    public static Review random(User reviewer, Movie toMovie) {
        return builder()
                .reviewer(reviewer)
                .movie(toMovie)
                .review(Generator.makeDescription())
                .stars(r.nextInt(5)+1)
                .build();
    }
}
