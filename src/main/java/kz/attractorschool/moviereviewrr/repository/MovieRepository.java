package kz.attractorschool.moviereviewrr.repository;

import kz.attractorschool.moviereviewrr.model.Movie;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends CrudRepository<Movie, String> {
    public List<Movie> findAll();
    public Iterable<Movie> findAll(Sort sort);

    public Iterable<Movie> findAllByReleaseYearGreaterThanEqual(int year, Sort sort);

    public Iterable<Movie> findAllByReleaseYearBetween(int year, int year2, Sort sort);

    @Query("{'releaseYear'} : { '$gte' : ?0, '$lte' : ?1 }")
    public Iterable<Movie> getMoviesBetween(int year, int year2, Sort sort);

    public Iterable<Movie> findFirstByTitle(String title);
}
