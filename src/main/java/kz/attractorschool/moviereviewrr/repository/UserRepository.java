package kz.attractorschool.moviereviewrr.repository;

import kz.attractorschool.moviereviewrr.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    public List<User> findAll();
    public Iterable<User> getById(String id);
    public Iterable<User> getByName(String name);
    public Iterable<User> getByEmail(String email);
    public boolean existsByEmail(String email);
}
