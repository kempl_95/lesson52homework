package kz.attractorschool.moviereviewrr.controller;

import kz.attractorschool.moviereviewrr.MovieReviewrrApplication;
import kz.attractorschool.moviereviewrr.model.Movie;
import kz.attractorschool.moviereviewrr.model.Review;
import kz.attractorschool.moviereviewrr.model.User;
import kz.attractorschool.moviereviewrr.repository.MovieRepository;
import kz.attractorschool.moviereviewrr.repository.ReviewRepository;
import kz.attractorschool.moviereviewrr.repository.UserRepository;
import kz.attractorschool.moviereviewrr.service.MovieService;
import kz.attractorschool.moviereviewrr.service.ReviewService;
import kz.attractorschool.moviereviewrr.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MainController{
    @Autowired
    MovieRepository movieRepository;

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;
    @Autowired
    MovieService movieService;
    @Autowired
    ReviewService reviewService;

    @GetMapping("/movie")
    public Iterable<Movie> getMovie() {
        Sort sort = Sort.by(Sort.Order.asc("title"));
        return movieRepository.findAll(sort);
    }

    @GetMapping("/movieNew/{year}")
    public Iterable<Movie> getMovie(@PathVariable("year") int year) {
        Sort sort = Sort.by(Sort.Order.asc("title"));
        return movieRepository.findAllByReleaseYearGreaterThanEqual(year ,sort);
    }

    @GetMapping("/movieNew/{year}/{year2}")
    public Iterable<Movie> getMovieBetween(@PathVariable("year") int year,
                                    @PathVariable("year2") int year2) {
        Sort sort = Sort.by(Sort.Order.asc("title"));
        return movieRepository.findAllByReleaseYearBetween(year, year2 ,sort);
    }
    @GetMapping("/movieNew2/{year}/{year2}")
    public Iterable<Movie> getMovieBetween2(@PathVariable("year") int year,
                                    @PathVariable("year2") int year2) {
        Sort sort = Sort.by(Sort.Order.asc("title"));
        return movieRepository.getMoviesBetween(year, year2 ,sort);
    }
    //Тестирую
    //ВНИМАНИЕ!!!!
    //В задании не указано какие методы надо реализовать, в связи с чем были сделано несколько просто для ознакомления.
        //Основной упор был сделан на задание 2. Microgram.
    @GetMapping("/userId={id}")
    public Iterable<User> getUserById(@PathVariable("id") String id) {
        return userService.getById(id);
    }
    @GetMapping("/userName={name}")
    public Iterable<User> getUserByName(@PathVariable("name") String name) {
        return userService.getByName(name);
    }
    @GetMapping("/userEmail={email}")
    public Iterable<User> getUserByEmail(@PathVariable("email") String email) {
        return userService.getByEmail(email);
    }
    @GetMapping("/movieTitle={title}")
    public Iterable<Movie> getMovieByTitle(@PathVariable("title") String title) {
        return movieService.getMovieByTitle(title);
    }
    @GetMapping("/reviewId={id}")
    public Iterable<Review> getReviewById(@PathVariable("id") String id) {
        return reviewService.getReviewById(id);
    }

}
