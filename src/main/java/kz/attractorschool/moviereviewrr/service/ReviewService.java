package kz.attractorschool.moviereviewrr.service;

import kz.attractorschool.moviereviewrr.model.Review;
import kz.attractorschool.moviereviewrr.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReviewService {
    @Autowired
    ReviewRepository reviewRepository;

    public Iterable<Review> getReviewById(String id){
        return reviewRepository.getById(id);
    }

}
