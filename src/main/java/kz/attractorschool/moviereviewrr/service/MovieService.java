package kz.attractorschool.moviereviewrr.service;

import kz.attractorschool.moviereviewrr.model.Movie;
import kz.attractorschool.moviereviewrr.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieService {
    @Autowired
    MovieRepository movieRepository;

    Movie movie;

    public void getMovieByTitle(){
        movieRepository.findFirstByTitle(movieRepository.findAll().get(1).getTitle());
        System.out.println("---------------------------------------");
    }
    public Iterable<Movie> getMovieByTitle(String title){
        return movieRepository.findFirstByTitle(title);
    }
}
