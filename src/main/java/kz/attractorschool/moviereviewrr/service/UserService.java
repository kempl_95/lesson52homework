package kz.attractorschool.moviereviewrr.service;

import kz.attractorschool.moviereviewrr.model.User;
import kz.attractorschool.moviereviewrr.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    User user;

    public List<User> getUserList(){
        return userRepository.findAll();
    }

    public boolean isUserExists(String email){
        return userRepository.existsByEmail(email);
    }
    public Iterable<User> getById(String id){
        return userRepository.getById(id);
    }
    public Iterable<User> getByName(String name){
        return userRepository.getByName(name);
    }
    public Iterable<User> getByEmail(String email){
        return userRepository.getByEmail(email);
    }

}
